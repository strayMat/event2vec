import numpy as np
import pandas as pd
import pytest
from scipy.sparse import load_npz

from event2vec.config import (
    COLNAME_PERSON,
    COLNAME_SOURCE_CODE,
    COLNAME_START,
    DIR2DATA,
)
from event2vec.cooccurrence_matrix_pandas import build_cooccurrence_matrix_pd
from event2vec.svd_ppmi import build_embeddings, build_ppmi, event2vec

expected_cooccurrence_matrix_pandas = np.array(
    [
        [0.0, 1.0, 0.0, 0.0, 0.0],
        [1.0, 2.0, 1.0, 2.0, 1.0],
        [0.0, 1.0, 0.0, 1.0, 0.0],
        [0.0, 2.0, 1.0, 0.0, 1.0],
        [0.0, 1.0, 0.0, 1.0, 0.0],
    ]
)
test_output_dir = DIR2DATA / "mock_data" / "results"


@pytest.mark.parametrize("max_patients_by_block", [5, 100])
def test_build_cooccurrence_matrix_pd(max_patients_by_block):
    path2mock_events = DIR2DATA / "mock_data" / "event.csv"
    events = pd.read_csv(path2mock_events, parse_dates=["start"])
    cooccurrence_matrix, event_count, label2ix = build_cooccurrence_matrix_pd(
        events,
        output_dir=test_output_dir,
        radius_in_days=30,
        window_orientation="center",
        max_patients_by_block=max_patients_by_block,
    )

    np.testing.assert_equal(
        expected_cooccurrence_matrix_pandas, cooccurrence_matrix.toarray()
    )
    assert event_count.sum() == len(events)
    assert len(event_count) == 5
    assert len(label2ix) == 5


@pytest.mark.parametrize("sparse", [True, False])
def test_build_embeddings(sparse):
    cooccurrence_matrix = load_npz(test_output_dir / "cooccurrence_matrix.npz")
    event_count = np.load(test_output_dir / "event_count.npy")
    ppmi = build_ppmi(
        cooccurrence_matrix=cooccurrence_matrix, event_count=event_count
    )
    embeddings = build_embeddings(ppmi=ppmi, sparse=sparse, d=3)
    assert embeddings.shape == (5, 3)


# @pytest.mark.parametrize("backend", ["pandas"])
def test_event2vec():
    path2mock_events = DIR2DATA / "mock_data" / "event.csv"
    events = pd.read_csv(path2mock_events, parse_dates=["start"])
    embeddings_df = event2vec(
        events=events,
        output_dir=test_output_dir,
        colname_concept="event_source_concept_id",
        window_orientation="center",
        window_radius_in_days=30,
        backend="pandas",
        d=3,
        smoothing_factor=0.75,
        k=1,
    )
    assert embeddings_df.shape == (3, 5)
    assert (
        test_output_dir / f"tuto_snds2vec_alpha=0.75_k=1_d=3.parquet"
    ).exists()
