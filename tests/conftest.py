"""Pytest configuration module"""
import pytest

from event2vec.datasets import get_small_cohort


@pytest.fixture(scope="session")
def mock_event_cohort():
    return get_small_cohort()
