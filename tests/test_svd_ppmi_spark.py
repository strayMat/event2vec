import sys

import numpy as np
import pytest

from event2vec.config import DIR2DATA
from event2vec.svd_ppmi import event2vec

expected_cooccurrence_matrix_pandas = np.array(
    [
        [0.0, 1.0, 0.0, 0.0, 1.0],
        [1.0, 0.0, 1.0, 0.0, 2.0],
        [0.0, 1.0, 0.0, 0.0, 1.0],
        [0.0, 0.0, 0.0, 0.0, 1.0],
        [1.0, 2.0, 1.0, 1.0, 2.0],
    ]
)
test_output_dir = DIR2DATA / "mock_data" / "results"


@pytest.mark.skipif(
    sys.version_info > (3, 7), reason="requires python3.7 to run pyspark"
)
@pytest.mark.parametrize(
    "matrix_type",
    ["numpy", "parquet"],
)
def test_build_cooccurrence_matrix_spark(matrix_type):
    from pyspark.sql import SparkSession

    from event2vec.cooccurrence_matrix_spark import (
        build_cooccurrence_matrix_spark,
    )

    sc = (
        SparkSession.builder.appName("hive")
        .config("spark.default.parallelism", 4)
        .getOrCreate()
    )

    path2mock_events = DIR2DATA / "mock_data" / "event.csv"
    events = sc.read.csv(
        str(path2mock_events), inferSchema=True, header=True
    ).cache()
    (
        cooccurrence_matrix,
        event_count,
        label2ix,
    ) = build_cooccurrence_matrix_spark(
        events,
        output_dir=test_output_dir,
        radius_in_days=30,
        matrix_type=matrix_type,
    )
    # Spark CountVectorizer is different from from sklearn CountVectorizer (that
    # is alphabetically sorted). So I need to resort the index and columns to be
    # the same than the expected matrix.
    alphabetical_order = np.sort(list(label2ix.keys()))
    decoder = {v: k for v, k in label2ix.items()}
    alphabetical_ix = [decoder[c] for c in alphabetical_order]
    cooccurrence_matrix_sorted_alphabetically = cooccurrence_matrix[
        alphabetical_ix
    ][:, alphabetical_ix]
    np.testing.assert_equal(
        expected_cooccurrence_matrix_pandas,
        cooccurrence_matrix_sorted_alphabetically,
    )
    assert event_count.sum() == events.count()
    assert len(event_count) == 5
    assert len(label2ix) == 5


@pytest.mark.skipif(
    sys.version_info > (3, 7), reason="requires python3.7 to run pyspark"
)
def test_event2vec():
    path2mock_events = DIR2DATA / "mock_data" / "event.csv"
    sc = (
        SparkSession.builder.appName("hive")
        .config("spark.default.parallelism", 4)
        .getOrCreate()
    )

    events = sc.read.csv(str(path2mock_events), inferSchema=True, header=True)
    embeddings_df = event2vec(
        events=events,
        output_dir=test_output_dir,
        colname_concept="event_source_concept_id",
        window_orientation="center",
        window_radius_in_days=30,
        backend="spark",
        d=3,
        smoothing_factor=0.75,
        k=1,
    )
    assert embeddings_df.shape == (3, 5)
    assert (
        test_output_dir / f"tuto_snds2vec_alpha=0.75_k=1_d=3.parquet"
    ).exists()
