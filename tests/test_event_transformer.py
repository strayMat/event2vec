import numpy as np
import pandas as pd
import polars as pl
import pytest
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import RandomizedSearchCV
from sklearn.pipeline import Pipeline
from sklearn.utils.validation import check_is_fitted

from event2vec.config import (
    COLNAME_FOLLOWUP_START,
    COLNAME_PERSON,
    COLNAME_SOURCE_CODE,
    COLNAME_START,
    PATH2SNDS_EMBEDDINGS,
)
from event2vec.event_transformer import (
    DemographicsTransformer,
    Event2vecFeaturizer,
    Event2vecPretrained,
    OneHotEvent,
    build_vocabulary,
    make_counts_pl,
    restrict_to_vocabulary,
)
from event2vec.utils import EventCohort, to_lazyframe

expected_one_hot = np.array([[0.0, 2.0, 1.0, 2.0], [2.0, 0.0, 0.0, 0.0]])
expected_decay = np.hstack(
    [
        expected_one_hot,
        np.array([[0.0, 0.00662, 0.0, 0.049787], [0.09957414, 0.0, 0.0, 0.0]]),
    ]
)


new_event = pd.DataFrame(
    {
        COLNAME_PERSON: [5, 5, 5, 6, 7, 7, 7],
        COLNAME_SOURCE_CODE: [
            "A04AA01",
            "A04AA01",
            "HBBD404",
            "OOV001",
            "H353",
            "H353",
            "H353",
        ],
        COLNAME_START: [
            "2021-01-05 00:00:00",
            "2021-01-05 00:00:00",
            "2021-01-05 00:00:00",
            "2021-01-05 00:00:00",
            "2021-01-05 00:00:00",
            "2021-01-05 00:00:00",
            "2021-01-05 00:00:00",
        ],
    },
)
new_event[COLNAME_START] = pd.to_datetime(new_event[COLNAME_START])
new_person = pd.DataFrame(
    {
        COLNAME_PERSON: [7, 6, 5],
        "y": [0, 1, 1],
        COLNAME_FOLLOWUP_START: [
            "2021-01-07 00:00:00",
            "2021-01-05 00:00:00",
            "2021-01-07 00:00:00",
        ],
    }
)
new_person[COLNAME_FOLLOWUP_START] = pd.to_datetime(
    new_person[COLNAME_FOLLOWUP_START]
)


@pytest.mark.parametrize(
    "lazy",
    [True, False],
)
def test_make_counts_pl(lazy):
    person = pl.DataFrame(new_person)
    event = pl.DataFrame(new_event).join(
        person.select([COLNAME_PERSON, COLNAME_FOLLOWUP_START]),
        on=COLNAME_PERSON,
    )
    if lazy:
        event = event.lazy()
    sparse_count = make_counts_pl(
        event=event,
        person_id=person.select(COLNAME_PERSON),
        decay_half_life_in_days=[0, 1],
        vocabulary=["A04AA01", "H353", "HBBD404", "OOV001", "OOV002"],
    )
    expected_count = np.matrix(
        [
            [0.0, 3.0, 0.0, 0.0, 0.0, 0.0, 0.40600585, 0.0, 0.0, 0.0],
            [0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0],
            [2.0, 0.0, 1.0, 0.0, 0.0, 0.27067057, 0.0, 0.13533528, 0.0, 0.0],
        ]
    )
    np.testing.assert_array_almost_equal(
        sparse_count.todense(), expected_count, decimal=5
    )


def test_DemographicsTransformer(
    mock_event_cohort,
):
    all_events = pd.concat([mock_event_cohort.event, new_event])
    expected_output = np.array([[0], [1]])
    demographic_transformer = DemographicsTransformer(
        event=all_events,
        colname_demographics=["gender"],
    )
    X = demographic_transformer.fit_transform(
        X=mock_event_cohort.person[[COLNAME_PERSON, "gender"]]
    )
    np.testing.assert_almost_equal(X, expected_output, decimal=5)


def test_DemographicsTransformer_none(
    mock_event_cohort,
):
    all_events = pd.concat([mock_event_cohort.event, new_event])
    demographic_transformer = DemographicsTransformer(
        event=all_events,
        colname_demographics=["gender"],
    )
    with pytest.raises(ValueError) as excinfo:
        X = demographic_transformer.fit_transform(
            X=mock_event_cohort.person[[COLNAME_PERSON]]
        )


@pytest.mark.parametrize(
    "half_lifes, expected_output",
    [
        ([0], np.hstack([np.array([[0], [1]]), expected_one_hot])),
        ([0, 1], np.hstack([np.array([[0], [1]]), expected_decay])),
    ],
)
def test_OneHotEvent_fit_transform(
    mock_event_cohort,
    expected_output,
    half_lifes,
):
    all_events = pd.concat([mock_event_cohort.event, new_event])
    one_hot_event = OneHotEvent(
        event=all_events,
        n_min_events=1,
        decay_half_life_in_days=half_lifes,
        colname_demographics=["gender"],
    )
    X = one_hot_event.fit_transform(
        X=mock_event_cohort.person[[COLNAME_PERSON, "gender"]]
    )

    np.testing.assert_almost_equal(X, expected_output, decimal=5)


@pytest.mark.parametrize(
    "lazy",
    [True, False],
)
def test_OneHotEvent_transform(mock_event_cohort: EventCohort, lazy):
    all_events = pd.concat([mock_event_cohort.event, new_event])
    if lazy:
        all_events = to_lazyframe(all_events)
    one_hot_event = OneHotEvent(event=all_events, n_min_events=1)
    one_hot_event.fit(X=mock_event_cohort.person[[COLNAME_PERSON]])
    X = one_hot_event.transform(
        new_person[[COLNAME_PERSON]],
    )

    expected_one_hot_new_event = np.array(
        [
            [0.0, 0.0, 3.0, 0.0],
            [0.0, 0.0, 0.0, 0.0],
            [2.0, 0.0, 0.0, 1.0],
        ]
    )
    np.testing.assert_equal(X, expected_one_hot_new_event)


@pytest.mark.parametrize(
    "lazy, cache",
    [(False, True), (False, False), (True, False)],
)
def test_Event2vec(mock_event_cohort, lazy, cache):
    all_events = pd.concat([mock_event_cohort.event, new_event])
    if lazy:
        all_events = to_lazyframe(all_events)
    featurizer = Event2vecFeaturizer(
        event=all_events,
        output_dir=None,
        colname_code=COLNAME_SOURCE_CODE,
        backend="pandas",
        d=3,
        n_min_events=1,
        decay_half_life_in_days=[0, 1],
        cache=cache,
    )
    X_train = mock_event_cohort.person[[COLNAME_PERSON]]
    featurizer.fit(X=X_train)

    X_test = new_person[[COLNAME_PERSON]]
    X = featurizer.transform(X_test)
    assert X.shape == (3, 6)


@pytest.mark.parametrize(
    "lazy",
    [True, False],
)
def test_Event2vecPretrained_fit_transform(mock_event_cohort, lazy):
    all_events = pd.concat([mock_event_cohort.event, new_event])
    if lazy:
        all_events = to_lazyframe(all_events)
    featurizer = Event2vecPretrained(
        event=all_events,
        embeddings=PATH2SNDS_EMBEDDINGS,
        n_min_events=1,
        decay_half_life_in_days=[0, 1],
    )
    X_train = mock_event_cohort.person[[COLNAME_PERSON]]
    featurizer.fit(X=X_train)

    X_test = new_person[[COLNAME_PERSON]]
    X = featurizer.transform(X_test)
    expected_embeddings_summed = np.array([-5.616094, 0.0, -5.678187])

    np.testing.assert_array_almost_equal(
        X.values.sum(axis=1), expected_embeddings_summed
    )

    assert X.shape == (3, 300)


@pytest.mark.parametrize(
    "lazy",
    [True, False],
)
def test_Event2vecPretrained_pipeline_fit_transform(mock_event_cohort, lazy):
    all_events = pd.concat([mock_event_cohort.event, new_event])
    if lazy:
        all_events = to_lazyframe(all_events)
    featurizer = Event2vecPretrained(
        event=all_events,
        embeddings=PATH2SNDS_EMBEDDINGS,
        n_min_events=1,
        decay_half_life_in_days=[0, 1],
    )
    est = LogisticRegression()
    pipeline = RandomizedSearchCV(
        Pipeline([("featurizer", featurizer), ("estimator", est)]),
        param_distributions={"estimator__C": [0.1, 1, 10]},
        cv=2,
        n_iter=2,
    )
    X_train = mock_event_cohort.person[[COLNAME_PERSON]]
    X_train_duplicate = X_train.copy()
    X_train_duplicate[COLNAME_PERSON] = [4, 5]
    X_train = pd.concat([X_train, X_train_duplicate])
    y = np.hstack(
        [mock_event_cohort.person["y"], mock_event_cohort.person["y"]]
    )
    pipeline.fit(X=X_train, y=y)
    check_is_fitted(pipeline)


def test_restrict_to_vocabulary(mock_event_cohort):
    restricted = restrict_to_vocabulary(
        mock_event_cohort.event, ["A04AA01", "HBBD404"]
    )
    assert restricted.shape[0] == 4


def test_build_vocabulary(mock_event_cohort):
    vocabulary = build_vocabulary(mock_event_cohort.event, n_min_events=2)
    assert set(vocabulary) == {"A04AA01", "HBBD404", "DAQL009"}
