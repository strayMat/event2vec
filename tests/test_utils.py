import pandas as pd
import polars as pl
import pytest

from event2vec.config import COLNAME_FOLLOWUP_START, COLNAME_PERSON
from event2vec.utils import (
    _get_window,
    check_colnames_subset,
    to,
    to_lazyframe,
    to_pandas,
    to_polars,
)


@pytest.mark.parametrize(
    "backend, expected_window_start",
    [("spark", -1296000), ("pandas", -15)],
)
def test_get_window(backend, expected_window_start):
    window_start, window_end = _get_window(
        radius_in_days=30, window_orientation="center", backend=backend
    )
    assert window_start == expected_window_start


mock_df = pd.DataFrame(
    {
        COLNAME_PERSON: [7, 6, 5],
        "y": [0, 1, 1],
        COLNAME_FOLLOWUP_START: [
            "2021-01-07 00:00:00",
            "2021-01-05 00:00:00",
            "2021-01-07 00:00:00",
        ],
    }
)


def test_to_lazyframe():
    assert isinstance(to_lazyframe(mock_df), pl.LazyFrame)


def test_to_pandas():
    assert isinstance(to_pandas(to_lazyframe(mock_df)), pd.DataFrame)
    assert isinstance(to_pandas(mock_df), pd.DataFrame)


def test_to_polars():
    assert isinstance(to_polars(mock_df), pl.DataFrame)
    assert isinstance(to_polars(to_lazyframe(mock_df)), pl.DataFrame)


def test_to():
    assert isinstance(to(mock_df, backend=type(mock_df)), pd.DataFrame)
    assert isinstance(to(mock_df, backend=pl.DataFrame), pl.DataFrame)
    assert isinstance(to(mock_df, backend=pl.LazyFrame), pl.LazyFrame)


@pytest.mark.parametrize(
    "colnames_subest, colnames, expected",
    [
        (["a", "b"], ["a", "b", "c"], True),
        (["a", "z"], ["a", "b", "c"], False),
    ],
)
def test_check_colnames_subset(colnames_subest, colnames, expected):
    assert check_colnames_subset(colnames_subest, colnames) == expected
