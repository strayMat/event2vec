These are referentiel files that serves as correspondances between codes:

All codes leading to these nomenclatures are available [here](https://gitlab.com/DREES_code/OSAM/snds2vec_analyse/tree/master/prepare_hierarchy_files).

- ccam_tree : from atih ccam hierarchy file
- icd10_tree : from [athena](athena.ohdsi.org/) icd10
- nabm_tree : from ir_bio
- ngap_tree : conciliated from IR_ACT_COD and IR_NAT_V
- atc_tree : from [athena](athena.ohdsi.org/) atc  
- IR_PHA_R: snds drugs referentiel file
- merged_cip7_to_atc: referentiel file built by Athemane Dahmouh with thesorimed and gers databases. Slightly edited with bdpm-rxnorm-atc mapping for missing codes.
