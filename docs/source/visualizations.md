# Embedding visualizations

With a Tsne projection in 2D, it is easy to visualize the high dimensional
embedding space.

The legend are the vocabulary of each code. For these embeddings, it corresponds
to different structured tables.

# SNDS embeddings

![](_static/embeddings/snds/snds_tsne_plot__metric=cosine_perplexity=30_niter=1000_rs=2.html)

<iframe src="_images/snds_tsne_plot__metric=cosine_perplexity=30_niter=1000_rs=2.html" height="900px" width="100%"></iframe>

# APHP embeddings

![](_static/embeddings/aphp_sample/aphp_tsne_plot__metric=cosine_perplexity=30_niter=1000_rs=2.html)

<iframe src="_images/aphp_tsne_plot__metric=cosine_perplexity=30_niter=1000_rs=2.html" height="900px" width="100%"></iframe>
