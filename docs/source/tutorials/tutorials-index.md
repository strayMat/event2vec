# Tutorials

```{eval-rst}
.. toctree::
   :maxdepth: 0

   1. Quick start <_1_tuto_event2vec>
   2. Event transformer & sklearn API <_2_event_transformers>
   3. K-NN with medical embeddings <_3_nearest_neighboors>
   4. Step by step guide <_4_tuto_step_by_step_event2vec>
```
