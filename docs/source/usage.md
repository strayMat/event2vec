# Usage

## Installation

**Requirements**: python3.7

You can install Event2Vec via [pip](https://pip.pypa.io/):

```shell script
pip install event2vec
pip install pyspark==2.4.3 # need to be installed separately
```

Or locally with poetry:

```shell script
make provision-environment # Note: installs ALL dependencies!
poetry run pip install pyspark==2.4.3 # need to be installed separately
```
