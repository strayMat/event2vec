```{include} readme.md

```

```{eval-rst}
.. toctree::
   :hidden:
   :maxdepth: 0

   readme
   Usage <usage>
   Tutorials <tutorials/tutorials-index>
   Embedding visualizations <visualizations>
   Development <development>
   License <license>
```

```{eval-rst}
.. role:: bash(code)
   :language: bash
   :class: highlight
```
