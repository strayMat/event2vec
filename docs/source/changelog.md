# Changelog

## 0.0.31 (2023-05-25)

- Added get_embeddings_from_url() to download the snds embeddings from gitlab.
- Added event_transformer.py containing the EventTransformer classes:
  `OneHotEvent`, `Event2vecFeaturizer` and  `Event2vecPretrained`. These classes
should be compatible with the sklearn API.
