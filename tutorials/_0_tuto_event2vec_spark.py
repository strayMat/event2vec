# %%
import json
import os

import numpy as np
from pyspark.sql import SparkSession

from event2vec.config import DIR2DATA, DIR2RESULTS
from event2vec.cooccurrence_matrix_spark import event2vec

# How-to embed a collection of coded medical events into continuous embeddings

# *Références:*
# - O. Levy et Y. Goldberg, « Neural Word Embedding as Implicit Matrix Factorization », p. 9., 2014
# - A. L. Beam et al., « Clinical Concept Embeddings Learned from Massive Sources of Multimodal Medical Data », arXiv:1804.01486 [cs, stat], avr. 2018.

sc = SparkSession.builder.getOrCreate()

output_dir = DIR2RESULTS / "cooccurrence_matrix"

path2mock_events = DIR2DATA / "mock_data" / "event.csv"
events = sc.read.csv(str(path2mock_events), inferSchema=True, header=True)

print(f"Number of events: {events.count()}")
vocabulary_size = events.select("event_source_concept_id").distinct().count()
print(f"Number of distinct events: {vocabulary_size}")

# SVD is possible only for $d<V$ where V is the vocabulary size. In this
# example, the vocabulary is of size . For real application d should be closer to 100.

# %%
alpha = 0.75
k = 1
d = 3

embeddings, label2ix = event2vec(
    events=events,
    output_dir=output_dir,
    colname_concept="event_source_concept_id",
    window_orientation="centered",
    window_radius_in_days=30,
    d=d,
    smoothing_factor=alpha,
    k=k,
)
# %%
embeddings_dict = {
    code: list(v) for (v, code) in zip(embeddings, label2ix.keys())
}
embeddings_dict
