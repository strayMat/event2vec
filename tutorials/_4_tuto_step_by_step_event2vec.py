# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.3'
#       jupytext_version: 1.13.8
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

# %%
import json
from datetime import datetime

import numpy as np
import pandas as pd
import scipy.sparse as sp

from event2vec.config import DIR2DATA, DIR2RESOURCES, DIR2RESULTS
from event2vec.cooccurrence_matrix_pandas import build_cooccurrence_matrix_pd
from event2vec.nomenclatures import get_concepts_labels
from event2vec.svd_ppmi import build_embeddings, build_ppmi

# %% [markdown]
# # Tutorial program
# In this tutorial, you will:
# - build a coocurrence matrix between different coded events from an EHR or
#   claim database formatted in an event-like long data table.
# - Compute a low dimensional embeddings of these medical concepts
# - visualize their projection in 2 dimension using tsne and plotly

# ### Motivation

# A cohort is a set of persons who satisfy one or more inclusion criteria for a
# duration of time, [Book of
# ohdsi](https://ohdsi.github.io/TheBookOfOhdsi/Cohorts.html#what-is-a-cohort).
# The most simple data representation of a cohort is a pair of two dataframes
# (person, events). The person dataframe is a wide dataframe with static
# information on the person such as its birthdate and death date. The event
# dataframe is a long dataframe with one row per event with a medical code and
# an associated timestamp.
# For each patient the individual sequence of events can be thought of as a
# sequence of tokens with a time-weighted distance between tokens. With this
# representation in mind, we can try to learn a low dimensional embedding of
# events with word2vec.

# This approach to medical concept embeddings has first been proposed by:
# [A. L. Beam et al., 2018, Clinical Concept Embeddings Learned from Massive
# Sources of Multimodal Medical
# Data](https://www.worldscientific.com/doi/abs/10.1142/9789811215636_0027).

# We propose here an implementation of this approach in python leveraging pyspark.


### The algorithm: [Neural Word Embedding as Implicit Matrix Factorization, Levy and Goldberg, 2014](https://papers.nips.cc/paper/2014/file/feab05aa91085b7a8012516bc3533958-Paper.pdf)

# Levy and Goldberg, 2014 showed that word2vec skipgram objective is equivalent
# to the factorization of the (shifted) PointWise Mutual Information
# matrix. This matrix is simply the cooccurence matrix between
# each pair of distinct event $PMI(c_i, c_j) = log\frac{P(c_i, c_j)}{P(c_i)P(c_j)}$:

# The main computation takes place by building the co-occurrences matrix
# $M=P(c_i, c_j)$ between distinct events of the medical database:
# - Initialize $M$, a $(V x V)$ matrix to 0, where V is the size of the
#   vocabulary ie. the number of distinct codes in the database,,
# - Choose a window size `window_size`,
# - Sort the events by patient and time,
# - Slide a centered window of size `window_size` centered on a code $c_i$ and
#   update $M$ by adding 1 to each cell $M_{i, j}$ where $c_j$ is in the window.

# ![cooccurrence_explained](/_static/imgs/cooccurrences.png)

# ## 1- Create a cooccurrence matrix from a cohort

# Load mock events
# %%
path2mock_events = DIR2DATA / "mock_data" / "event.csv"

events = pd.read_csv(path2mock_events, parse_dates=["start"])

print(events.count())
print(events["person_id"].nunique())

events.head(5)
# %%
cooccurrence_matrix, event_count, label2ix = build_cooccurrence_matrix_pd(
    events,
    output_dir=DIR2RESULTS / "cooccurrence_matrix",
    radius_in_days=30,
)
# %% [markdown]
# ## 2 - Visualize the cooccurrence matrix
# Load the precomputed cooccurrence matrix and map the codes to their labels.
# %%
path2vocabulary = DIR2RESULTS / "cooccurrence_matrix" / "vocabulary.json"
cooccurrence_matrix = sp.load_npz(
    DIR2RESULTS / "cooccurrence_matrix" / "cooccurrence_matrix.npz"
).todense()
with open(path2vocabulary, "rb") as f:
    label2ix = json.load(f)
event_count = np.load(DIR2RESULTS / "cooccurrence_matrix" / "event_count.npy")

plain_labels = list(label2ix.keys())
# %%
import plotly
import plotly.graph_objects as go

data = [
    {
        "z": cooccurrence_matrix,
        "x": plain_labels,
        "y": plain_labels,
        # colorscale='Viridis'
        "type": "heatmap",
        "colorscale": [  # max is 25e6, it corresponds to 1. in following colormap, we could think of a better way maybe
            [0, "rgb(0, 0, 0)"],  # 0
            [1.0 / 10000, "rgb(50, 50, 50)"],  # 2500
            [1.0 / 2000, "rgb(100, 100, 100)"],  # 2500
            [1.0 / 200, "rgb(150, 150, 150)"],  # 12.5K
            [1.0 / 20, "rgb(200, 200, 200)"],  # 125K -> 99% percentiles
            [1.0, "rgb(250, 250, 250)"],  # max=25e6
        ],
        "colorbar": {
            "tick0": 0,
            "tickmode": "array",
            "tickvals": [0, 2500, 12500, 125000, 1250000],
        },
    }
]

layout = {"title": f"CoOccurrence Matrix"}
fig = {"data": data, "layout": layout}
config = {"scrollZoom": True}
plotly.offline.iplot(fig, filename="demo.html", config=config)
# %% [markdown]
# ## 3- Create the embeddings

# From the Cooccurrence matrix, we can compute the Pointwise Mutual Information
# then factorize it with SVD to get the embeddings.

# The PMI matrix is the cooccurrence matrix between events normalized by the the
# product of probabilities of each event, then shifted by a parameter log(k)
# %%
alpha = 0.75
k = 1
# ppmi with a shift parameter to 1 (no shift when taking the log)
ppmi = build_ppmi(
    cooccurrence_matrix=cooccurrence_matrix,
    event_count=event_count,
    smoothing_factor=alpha,
    k=1,
)
sparsity = np.sum(ppmi == 0) / ppmi.shape[0] ** 2
print(f"Sparsity of the PMI: {sparsity:.4f}")

# %% [markdown]
# Perform the SVD and truncate at dimension d, return the embeddings as the mean of the context and word
# vectors : $embeddings= U_d \cdot \sqrt{S_d} + V_d \cdot \sqrt{S_d}$
# %%
d = 5
event_embeddings_as_matrix = build_embeddings(
    ppmi=ppmi, d=d, sparse=False, window_orientation="center"
)

path2emb = (
    DIR2RESULTS
    / "cooccurrence_matrix"
    / f"tuto_snds2vec_alpha={alpha}_k={k}_d={d}.parquet"
)
embeddings_dict = {
    k: v for k, v in zip(label2ix.keys(), np.array(event_embeddings_as_matrix))
}
embeddings_df = pd.DataFrame.from_dict(embeddings_dict, orient="columns")
embeddings_df.to_parquet(path2emb)


# %% [markdown]
# ## 4 - Visualize the embeddings

# To visualize in two dimension the embeddings, we can use t-SNE, a parametric
# gaussian dimension reduction favoring the apparition of groups.
# %%
from sklearn.decomposition import PCA, TruncatedSVD
from sklearn.manifold import TSNE

codes = list(embeddings_dict.keys())
codes = [c.split(":")[1] for c in codes]
embeddings_values = list(embeddings_dict.values())
embeddings_mat = np.matrix(embeddings_values)
concepts_labels = get_concepts_labels(
    codes, DIR2RESOURCES
)  # .set_index('concept_code')
# %%
concepts_labels["concept_terminology"].value_counts()
color_map = {
    "atc7": "limegreen",
    "cim10": "deepskyblue",
    "ccam": "firebrick",
    "nabm": "orchid",
    "ngap": "yellow",
    "unknown": "grey",
}
concepts_labels["plotted_label"] = concepts_labels.apply(
    lambda x: x["concept_code"] + " : " + x["concept_name"], axis=1
)  # ['concept_code']= .to_dict()
concepts_labels["plotted_color"] = (
    concepts_labels["concept_terminology"]
    .map(lambda x: color_map[x])
    .to_list()
)
# %% [markdown]
# Fit the TSNE
# %%
perplexity = 30
n_iter = 250  # use 1000 for better results
metric = "cosine"
tsne = TSNE(
    n_components=2,
    metric=metric,
    perplexity=perplexity,
    early_exaggeration=15,
    n_iter=n_iter,
    random_state=2,
)  # n_jobs=40,
X = tsne.fit_transform(embeddings_mat)
# %% [markdown]
# ## Interactive plot
# %%
coordinates = pd.DataFrame({"concept_code": codes, "x": X[:, 0], "y": X[:, 1]})
plotted_concepts = concepts_labels.merge(
    coordinates, on="concept_code", how="inner"
)
print(plotted_concepts.shape)
print(concepts_labels.shape)

vocabs = ["atc7", "ccam", "cim10", "nabm"]

import plotly
import plotly.graph_objects as go

fig = go.Figure(
    layout=dict(
        # title="{}".format('snds2vec'),
        autosize=True,
        width=1000,
        height=1000,
        xaxis={"title": "x", "showticklabels": True},
        yaxis={"title": "y", "showticklabels": False},
        margin={"l": 0, "b": 0, "t": 50, "r": 0},
        showlegend=True,
        hovermode="closest",
        dragmode="pan",
    )
)

for vocab in vocabs:
    vocab_data = plotted_concepts.loc[
        plotted_concepts["concept_terminology"] == vocab, :
    ]
    fig.add_trace(
        go.Scattergl(
            x=vocab_data["x"],
            y=vocab_data["y"],
            legendgroup="terminology",
            name=vocab,
            text=vocab_data["plotted_label"],
            mode="markers",
            opacity=0.8,
            marker={"size": 6, "color": vocab_data["plotted_color"]},
        )
    )

fig.update_layout(legend=dict(x=0, y=1))
config = {"scrollZoom": True}

fig.show(renderer="jupyterlab", config=config)
plotly.offline.iplot(
    fig, filename="{}".format("tuto_tsne_snds2vec"), config=config
)

# %%
