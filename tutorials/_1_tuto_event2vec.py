# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.3'
#       jupytext_version: 1.13.8
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

# %%
import pandas as pd

from event2vec.config import (
    COLNAME_PERSON,
    COLNAME_SOURCE_CODE,
    COLNAME_START,
    DIR2DATA,
    DIR2RESULTS,
)
from event2vec.svd_ppmi import event2vec

# %% [markdown]
# # How-to embed a collection of coded medical events into continuous embeddings

# *Références:*
# - O. Levy et Y. Goldberg, « Neural Word Embedding as Implicit Matrix Factorization », p. 9., 2014
# - A. L. Beam et al., « Clinical Concept Embeddings Learned from Massive Sources of Multimodal Medical Data », arXiv:1804.01486 [cs, stat], avr. 2018.
# %%
output_dir = DIR2RESULTS / "cooccurrence_matrix"

path2mock_events = DIR2DATA / "mock_data" / "event.csv"
events = pd.read_csv(path2mock_events, parse_dates=["start"])

print(f"Number of events: {events.shape[0]}")
vocabulary_size = events["event_source_concept_id"].nunique()
print(f"Number of distinct events: {vocabulary_size}")
events[[COLNAME_PERSON, COLNAME_START, COLNAME_SOURCE_CODE]].sample(5).head()
# %% [markdown]
# SVD is possible only for $d<V$ where V is the vocabulary size. In this
# example, the vocabulary is of size . For real application d should be closer to 100.

# %%
alpha = 0.75
k = 1
d = 3

embeddings = event2vec(
    events=events,
    output_dir=output_dir,
    colname_concept="event_source_concept_id",
    window_orientation="center",
    window_radius_in_days=30,
    d=d,
    smoothing_factor=alpha,
    k=k,
    backend="pandas",
)
embeddings
