# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.3'
#       jupytext_version: 1.13.8
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

# %%
import pandas as pd
from IPython.display import display
from sklearn.linear_model import LogisticRegression
from sklearn.pipeline import Pipeline

from event2vec.config import *
from event2vec.datasets import get_small_cohort
from event2vec.event_transformer import Event2vecPretrained
from event2vec.utils import get_embeddings_from_url

# %% [markdown]
# # Use sklearn pipeline
# ## Load mock data
# %%
event_cohort = get_small_cohort()
print("Person table:")
display(event_cohort.person.head())
print("Event table:")
display(event_cohort.event.head())
# %% [markdown]
# This tutorial will show you how to use the `Event2vecPretrained`
# transformer to using pretrained embeddings to embed patient vectors and plug
# the representations into a sklearn classifier.

# ## Load pretrained embeddings
# %%
snds_url = "https://gitlab.com/strayMat/event2vec/-/raw/main/data/results/snds/echantillon_mid_grain_r=90-centered2019-12-05_19:11:27.parquet"
if not PATH2SNDS_EMBEDDINGS.exists():
    snds_embeddings = get_embeddings_from_url(snds_url)
    PATH2SNDS_EMBEDDINGS.mkdir(parents=True, exist_ok=True)
    snds_embeddings.to_parquet(PATH2SNDS_EMBEDDINGS)
else:
    snds_embeddings = pd.read_parquet(PATH2SNDS_EMBEDDINGS)
# %% [markdown]
# ## Instantiate the transformer and the pipeline

# Note that you need to pass the event table to the transformer. If you intend
# to predict on new test events, you need to pass the test event table to the transformer
# at instanciation time as well.

# Other EventTransformer available are : `OneHotEvent` and `Event2vecFeaturizer`.
#  %%
event_transformer = Event2vecPretrained(
    event=event_cohort.event,
    embeddings=snds_embeddings,
    n_min_events=1,
)
pipeline = Pipeline(
    [
        ("event_transformer", event_transformer),
        ("estimator", LogisticRegression()),
    ]
)
# %% [markdown]
# ## Fit the pipeline
# %%
pipeline.fit(
    X=event_cohort.person[[COLNAME_PERSON]],
    y=event_cohort.person["y"],
)
# %% [markdown]
# ## Predict
# %%
y_pred = pipeline.predict_proba(event_cohort.person[[COLNAME_PERSON]])
y_pred
