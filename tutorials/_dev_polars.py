# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.3'
#       jupytext_version: 1.13.8
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

# %%
import numpy as np
import polars as pl
from sklearn.feature_extraction.text import CountVectorizer

from event2vec.config import (
    COLNAME_FOLLOWUP_START,
    COLNAME_PERSON,
    COLNAME_SOURCE_CODE,
    COLNAME_START,
    DIR2DATA,
    DIR2RESULTS,
)
from event2vec.datasets import get_small_cohort

output_dir = DIR2RESULTS / "cooccurrence_matrix"

path2mock_events = DIR2DATA / "mock_data" / "event.csv"
event_cohort = get_small_cohort()


event = pl.DataFrame(event_cohort.event)
person = pl.DataFrame(event_cohort.person)
# print(f"Number of events: {events.shape[0]}")
# ocabulary_size = events["event_source_concept_id"].n_unique()
# (f"Number of distinct events: {vocabulary_size}")
# events[[COLNAME_PERSON, COLNAME_START, COLNAME_SOURCE_CODE]].sample(5).head()
# %%
radius_in_days = 30
window_orientation = "center"
colname_concept = COLNAME_SOURCE_CODE

vocabulary_ = event.select(colname_concept).unique()

events_sorted = event.sort(by=["person_id", "start"])
events_in_window = events_sorted.groupby_dynamic(
    "start", offset="-15d", every="30d", by="person_id", closed="both"
).agg(pl.col(colname_concept).alias("context"))
# %% TODO: here I explode the memory because I am leaving the polars world TODO:
# A solution is to dump to parquet, then process by batch.
# WARNING: there is a pb because polars does not keep the target word (center of
# context).
event_contexts = events_in_window[COLNAME_SOURCE_CODE].to_list()
transformer = CountVectorizer(
    analyzer=lambda x: iter(x),
    vocabulary=vocabulary_,
    lowercase=False,
)
sparse_counts = transformer.fit_transform(event_contexts)

# %%
# develop code for event_transformer
from event2vec.config import COLNAME_VALUE
from event2vec.event_transformer import make_counts_pl

X = make_counts_pl(event=event, person_id=person.select(COLNAME_PERSON))
X
# %%
event = person.select(COLNAME_PERSON).join(
    event,
    on=COLNAME_PERSON,
    how="inner",
)

event = event.with_columns(pl.lit(1).alias(COLNAME_VALUE))
event = event.with_columns(
    (
        (pl.col(COLNAME_FOLLOWUP_START) - pl.col(COLNAME_START)).dt.seconds()
        / (24 * 3600)
    ).alias("delta_to_followup")
)

# %%
event.with_columns(
    (np.exp(-pl.col("delta_to_followup") / 6)).alias(COLNAME_VALUE)
)

# %%
event.pivot(
    index=COLNAME_PERSON,
    columns=COLNAME_SOURCE_CODE,
    values=COLNAME_VALUE,
    aggregate_function="sum",
).fill_nan(0)
