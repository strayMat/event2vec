# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.3'
#       jupytext_version: 1.13.8
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---


import pandas as pd

from event2vec.concept_proximity import (
    get_closest_nn,
    get_closest_nn_by_vocabulary,
)
from event2vec.config import DIR2RESULTS

pd.set_option("display.max_colwidth", 1000)

# %% [markdown]
# # Compute nearest neighbors in embedding space

# The embeddings defines a distance between concepts. We can use this distance
# to find the nearest neighbors of a given concept.
# Eg. for, the concept "E101" (diabetes mellitus with acidocetose) we can find the 10 nearest
# neighbors in the SNDS embedding space.

# %% [markdown]
# ## Load the embeddings
# %%
dir2snds_embeddings = DIR2RESULTS / "snds"

concept_labels = pd.read_csv(dir2snds_embeddings / "concept_labels.csv")
if "concept_id" not in concept_labels.columns:
    concept_labels["concept_id"] = concept_labels["concept_code"]
concept_labels["concept_name"] = concept_labels[
    "concept_name"
].str.capitalize()
snds_embeddings = pd.read_parquet(
    dir2snds_embeddings
    / "echantillon_mid_grain_r=90-centered2019-12-05_19:11:27.parquet"
).to_dict(orient="list")
# %% [markdown]
# ## Compute the 10 closest concepts
# %%
k = 50
source_concept_code = "I210"  # Heart failure
# For SNDS
top_k_concepts = get_closest_nn(
    source_concept_code=source_concept_code,
    embedding_dict=snds_embeddings,
    concept_labels=concept_labels,
    k=k,
)
# Pretty printing
top_k_concepts["similarity"] = top_k_concepts["similarity"].round(3)
top_k_concepts["concept_code"] = (
    top_k_concepts["vocabulary_id"] + ":" + top_k_concepts["concept_code"]
)
top_k_concepts[["concept_code", "concept_name", "similarity"]]
# %%
# deliver the top similar concepts for each categoriess

# %% [markdown]
# We can do the same for the embeddings induced by the APHP extract
top_2_concepts_per_vocabulary = get_closest_nn_by_vocabulary(
    source_concept_code=source_concept_code,
    embedding_dict=snds_embeddings,
    concept_labels=concept_labels,
    k=2,
)
top_2_concepts_per_vocabulary
print(
    top_2_concepts_per_vocabulary.loc[
        top_2_concepts_per_vocabulary["vocabulary_id"] != "unknown"
    ]
    .set_index("concept_code")
    .to_latex()
)
# ## Aphp embeddings
# %%

# dir2aphp_embeddings = DIR2RESULTS / "omop_sample_4tables_90d"

# aphp_concept_labels = pd.read_csv(dir2aphp_embeddings / "concept_labels.csv")
# aphp_concept_labels["concept_id"] = aphp_concept_labels["concept_id"].astype(
#     str
# )
# with open(
#     dir2aphp_embeddings / "omop_200K_sample_4tables_alpha=0.75_k=1_d=150.json",
# ) as f:
#     aphp_embeddings = json.load(f)

# %%
# k = 10

# source_concept_code = "E101"
# # For APHP
# top_k_concepts = get_closest_nn(
#     source_concept_code=source_concept_code,
#     embedding_dict=aphp_embeddings,
#     concept_labels=aphp_concept_labels,
#     k=k,
# )
# # pretty printing
# top_k_concepts["vocabulary_id"] = top_k_concepts["vocabulary_id"].str.replace(
#     "APHP - ORBIS - ", ""
# )
# top_k_concepts["similarity"] = top_k_concepts["similarity"].round(2)
# top_k_concepts["concept_code"] = (
#     top_k_concepts["vocabulary_id"] + ":" + top_k_concepts["concept_code"]
# )
# print(
#     top_k_concepts[["concept_code", "concept_name", "similarity"]].to_latex(
#         index=False
#     )
# )

# %%
# top_k_concepts.loc[top_k_concepts["vocabulary_id"] != "APHP - ORBIS - cim10"]
